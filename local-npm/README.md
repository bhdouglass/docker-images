# Unofficial Docker image for local-npm

Docker image for local-npm using a [fork](https://github.com/ashubham/local-npm)
that supports recent versions of node.
